﻿using MailKit.Net.Smtp;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public class EmailService
    {
        private readonly ThreadContext _context;
        private const string AdminEmail = "";
        private const string AdminPassword = "";
        public EmailService(ThreadContext context)
        {
            _context = context;
        }
        public async Task LikePostNotification(int postId, int userId)
        {
            var post = _context.Posts.Find(postId);
            var user = _context.Users.Find(userId);
            var postOwner = _context.Users.Find(post.AuthorId);

            string subject = "Post Like Notification";
            string message = $"Your post {post.Body} was liked by {user.UserName}";
            await SendEmailAsync(postOwner.Email, subject, message);
        } 

        public async Task SharePost(SharePostDTO sharePost)
        {
            var post = await _context.Posts.Include(post => post.Author).Include(post => post.Preview)
                                          .FirstOrDefaultAsync(p => p.Id == sharePost.PostId);
            var currentUser = _context.Users.Find(sharePost.CurrentUserId);

            byte[] byteArray = null;

            var email = sharePost.Email;
            var subject = $"{currentUser.UserName} shared a post";

            if (post.Preview != null)
            {
                HttpClient client = new HttpClient();

                byteArray = await client.GetByteArrayAsync(post.Preview.URL);
            }

            await SendEmailAsync(email, subject, post.Body, byteArray);
        }
        public async Task SendEmailAsync(string email, string subject, string message, byte[] attachmentArray = null)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("Site administration", AdminEmail));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;

            if (attachmentArray == null)
            {
                emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = message
                };
            }
            else
            {
                var multipart = new Multipart("mixed");
                multipart.Add(new TextPart(MimeKit.Text.TextFormat.Html)
                {
                    Text = message
                });

                var attachment = new MimePart("image", "gif")
                {
                    Content = new MimeContent(new MemoryStream(attachmentArray), ContentEncoding.Default),
                    ContentDisposition = new ContentDisposition(ContentDisposition.Attachment),
                    ContentTransferEncoding = ContentEncoding.Base64
                };

                multipart.Add(attachment);

                emailMessage.Body = multipart;
            }

            using (var client = new SmtpClient())
            {
                await client.ConnectAsync("smtp.gmail.com", 25, false);

                try
                {
                    await client.AuthenticateAsync(AdminEmail, AdminPassword);
                }
                catch (MailKit.Security.AuthenticationException) 
                {
                    return;
                }

                await client.SendAsync(emailMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}
