﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        public CommentService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            return _mapper.Map<CommentDTO>(createdComment);
        }
        private async Task<Comment> GetCommentByIdInternal(int id)
        {
            var comment = await _context.Comments.Include(c => c.Author).FirstOrDefaultAsync(p => p.Id == id);
            return comment;
        }
        public async Task UpdateComment(CommentDTO commentDto)
        {
            var commentEntity = await GetCommentByIdInternal(commentDto.Id);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(User), commentDto.Id);
            }

            var timeNow = DateTime.Now;

            commentEntity.Id = commentDto.Id;
            commentEntity.Body = commentDto.Body;
            commentEntity.UpdatedAt = timeNow;

            _context.Comments.Update(commentEntity);
            await _context.SaveChangesAsync();
        }
        public async Task DeleteComment(int commentId)
        {
            var commentEntity = await _context.Comments.Include(c => c.Reactions).FirstOrDefaultAsync(c => c.Id == commentId);

            if (commentEntity == null)
            {
                throw new NotFoundException(nameof(User), commentId);
            }

            _context.Comments.Remove(commentEntity);
            await _context.SaveChangesAsync();
        }
    }
}
