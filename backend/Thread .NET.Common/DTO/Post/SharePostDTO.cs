﻿namespace Thread_.NET.Common.DTO.Post
{
    public sealed class SharePostDTO
    {
        public int CurrentUserId { get; set; }
        public int PostId { get; set; }
        public string Email { get; set; }
    }
}
