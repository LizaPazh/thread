﻿using FluentValidation;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public class UserDTOValidator<T> : AbstractValidator<T> where T : UserDTO
    {
        public UserDTOValidator()
        {
            RuleFor(u => u.Email).EmailAddress().WithMessage("It is invalid email address");
            RuleFor(u => u.UserName)
                .NotEmpty()
                    .WithMessage("Username is mandatory.")
                .MinimumLength(3)
                    .WithMessage("Username should be minimum 3 character.");
        }
       
    }
    public sealed class UserRegisterDTOValidator : UserDTOValidator<UserRegisterDTO>
    {
        public UserRegisterDTOValidator()
        {
            RuleFor(u => u.Password)
                .Length(4, 16)
                .WithMessage("Password must be from 4 to 16 characters.");
        }
    }
}
