﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly EmailService _emailService;
        public PostsController(PostService postService, LikeService likeService, EmailService emailService)
        {
            _postService = postService;
            _likeService = likeService;
            _emailService = emailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            return Ok(await _postService.GetAllPosts());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<PostDTO>> GetById(int id)
        {
            return Ok(await _postService.GetPostById(id));
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePost(reaction);
            return Ok();
        }
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] PostDTO post)
        {
            await _postService.UpdatePost(post);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            await _postService.DeletePost(id);
            return NoContent();
        }

        [HttpPost("share")]
        public async Task<IActionResult> Share(SharePostDTO sharePostDTO)
        {
            await _emailService.SharePost(sharePostDTO);
            return Ok();
        }
    }
}