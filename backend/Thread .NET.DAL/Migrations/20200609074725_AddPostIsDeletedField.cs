﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddPostIsDeletedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Posts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "IsLike", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7941), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7945) });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(6667), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7115), 18 },
                    { 2, 13, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7574), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7591), 11 },
                    { 3, 2, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7625), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7630), 13 },
                    { 4, 2, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7653), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7658), 4 },
                    { 5, 5, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7679), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7684), 18 },
                    { 6, 6, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7709), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7714), 1 },
                    { 7, 6, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7734), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7738), 18 },
                    { 8, 18, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7760), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7764), 14 },
                    { 9, 16, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7786), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7791), 2 },
                    { 20, 9, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8067), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8072), 7 },
                    { 11, 17, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7837), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7842), 14 },
                    { 19, 4, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8042), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8046), 21 },
                    { 18, 5, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8017), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8021), 20 },
                    { 17, 4, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7991), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7996), 6 },
                    { 10, 4, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7812), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7816), 12 },
                    { 14, 10, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7915), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7919), 3 },
                    { 13, 15, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7889), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7893), 19 },
                    { 12, 13, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7863), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7867), 6 },
                    { 16, 17, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7967), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7971), 15 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Modi nulla nisi sunt beatae modi minus.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(6515), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(6945) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Consequatur et magnam adipisci deserunt voluptas ullam.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7509), 2, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7527) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Suscipit aliquam similique eos mollitia tempore nihil.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7608), 20, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7615) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Qui quam velit odit ut harum sunt mollitia sed.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7740), 9, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7746) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Incidunt est ut id odit accusantium animi et.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7812), 2, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7818) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Distinctio velit delectus ad atque aut voluptate.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7878), 11, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7884) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Eveniet vero iure excepturi ratione modi a ut cumque praesentium.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7953), 5, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7959) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Accusantium a mollitia asperiores error in deserunt dolores.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8020), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8025) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Placeat et alias ex sunt fuga quibusdam quia officiis.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8144), 7, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8150) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Debitis explicabo sapiente et rerum ut blanditiis mollitia atque.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8217), 12, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8223) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Ipsam rerum voluptatibus voluptas ea rerum voluptatum quaerat amet qui.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8293), 5, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8299) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Animi quia beatae nam.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8347), 7, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8352) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Eligendi quia porro eveniet id animi hic voluptatem sit architecto.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8420), 6, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8426) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Veritatis ipsa sed facilis tenetur.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8520), 19, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8526) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Architecto similique exercitationem suscipit quisquam.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8590), 1, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8597) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Autem recusandae voluptatum.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8641), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8646) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut ipsum vitae labore et.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8701), 5, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8706) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Fugit voluptatem laudantium.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8747), 4, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8752) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Ut iusto quo nostrum voluptate est dolorum.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8810), 9, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8815) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Rerum voluptatem repudiandae sed consequatur eos nulla.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8869), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8875) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(3822), "https://s3.amazonaws.com/uifaces/faces/twitter/heycamtaylor/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7830), "https://s3.amazonaws.com/uifaces/faces/twitter/arashmanteghi/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7852) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7928), "https://s3.amazonaws.com/uifaces/faces/twitter/sprayaga/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7958), "https://s3.amazonaws.com/uifaces/faces/twitter/josemarques/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7962) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7982), "https://s3.amazonaws.com/uifaces/faces/twitter/thatonetommy/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7987) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8007), "https://s3.amazonaws.com/uifaces/faces/twitter/tanveerrao/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8011) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8030), "https://s3.amazonaws.com/uifaces/faces/twitter/peejfancher/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8054), "https://s3.amazonaws.com/uifaces/faces/twitter/scrapdnb/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8058) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8080), "https://s3.amazonaws.com/uifaces/faces/twitter/mrebay007/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8085) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8104), "https://s3.amazonaws.com/uifaces/faces/twitter/nicklacke/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8109) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8127), "https://s3.amazonaws.com/uifaces/faces/twitter/ceekaytweet/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8132) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8151), "https://s3.amazonaws.com/uifaces/faces/twitter/mrxloka/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8156) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8174), "https://s3.amazonaws.com/uifaces/faces/twitter/eyronn/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8179) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8196), "https://s3.amazonaws.com/uifaces/faces/twitter/joelhelin/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8201) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8219), "https://s3.amazonaws.com/uifaces/faces/twitter/bertboerland/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8223) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8243), "https://s3.amazonaws.com/uifaces/faces/twitter/suprb/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8265), "https://s3.amazonaws.com/uifaces/faces/twitter/d33pthought/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8289), "https://s3.amazonaws.com/uifaces/faces/twitter/artem_kostenko/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8294) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8313), "https://s3.amazonaws.com/uifaces/faces/twitter/attacks/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8317) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8335), "https://s3.amazonaws.com/uifaces/faces/twitter/croakx/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(998), "https://picsum.photos/640/480/?image=630", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(1764) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2121), "https://picsum.photos/640/480/?image=84", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2201) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2227), "https://picsum.photos/640/480/?image=390", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2232) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2252), "https://picsum.photos/640/480/?image=319", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2257) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2276), "https://picsum.photos/640/480/?image=863", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2391), "https://picsum.photos/640/480/?image=88", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2396) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2416), "https://picsum.photos/640/480/?image=482", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2438), "https://picsum.photos/640/480/?image=1021", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2443) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2461), "https://picsum.photos/640/480/?image=506", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2465) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2482), "https://picsum.photos/640/480/?image=946", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2487) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2504), "https://picsum.photos/640/480/?image=380", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2508) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2525), "https://picsum.photos/640/480/?image=656", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2547), "https://picsum.photos/640/480/?image=343", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2551) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2568), "https://picsum.photos/640/480/?image=621", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2573) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2590), "https://picsum.photos/640/480/?image=708", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2595) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2612), "https://picsum.photos/640/480/?image=118", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2617) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2634), "https://picsum.photos/640/480/?image=697", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2655), "https://picsum.photos/640/480/?image=113", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2659) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2954), "https://picsum.photos/640/480/?image=90", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2959) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2980), "https://picsum.photos/640/480/?image=559", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2984) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 4, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9703), true, 18, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9708), 1 },
                    { 5, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9730), false, 10, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9735), 9 },
                    { 6, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9758), true, 5, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9762), 20 },
                    { 2, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9622), false, 19, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9638), 16 },
                    { 8, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9811), false, 15, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9816), 10 },
                    { 9, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9838), false, 16, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9843), 8 },
                    { 10, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9864), true, 6, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9869), 1 },
                    { 7, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9785), true, 1, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9789), 16 },
                    { 3, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9674), false, 20, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9679), 1 },
                    { 15, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9995), false, 8, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9999), 4 },
                    { 1, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(8601), false, 1, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9042), 20 },
                    { 14, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9965), true, 13, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9970), 10 },
                    { 11, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9889), false, 6, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9894), 9 },
                    { 16, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(21), false, 13, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(26), 2 },
                    { 17, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(46), true, 18, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(51), 3 },
                    { 18, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(71), false, 10, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(76), 14 },
                    { 19, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(96), false, 10, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(101), 4 },
                    { 20, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(122), true, 8, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(126), 1 },
                    { 13, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9940), false, 20, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9945), 19 },
                    { 12, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9915), true, 13, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9919), 8 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Est veniam vero error sequi dignissimos iure occaecati.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(1574), 32, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(2042) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Cupiditate et eaque nulla et molestiae nostrum sed qui.
Repellendus qui dolore repellendus nemo quia veritatis architecto nihil excepturi.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5179), 32, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5211) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Voluptatem debitis enim doloremque.
Voluptatem expedita et ut quis perspiciatis occaecati.
Minima vel amet sed.
Vel quasi dolorem.
Distinctio vitae ut occaecati aut eius et dolor non enim.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5575), 28, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5584) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Sit molestias quia rerum magni delectus minima.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5660), 30, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5666) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Quae voluptas totam ut aliquam sunt sit et numquam.
Soluta doloremque neque odio.
Autem beatae enim consequuntur culpa illum dolorem dolorem laborum accusamus.
Earum velit harum neque ipsam eligendi cumque nemo.
Ipsum impedit earum perferendis.
Facilis eius suscipit dolorem dolorem qui voluptatum eligendi doloremque.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5979), 30, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5987) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Ut reiciendis eius nesciunt vero in sint.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6056), 33, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6062) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Doloribus sit vero ut omnis vitae dolores aliquam corporis itaque.
Corrupti nisi temporibus est voluptatem magnam ipsum.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6228), 23, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6234) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Voluptas hic perferendis at perspiciatis et. Dolorem est veniam hic. Vel dolorum molestiae enim ea quis ut modi ullam.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7152), 28, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7167) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Magnam vel est totam ipsam ab dolorem et. Ipsam placeat tempora officia possimus fugit aut. Quia reprehenderit tenetur repudiandae facilis dolorem ut quod qui officia.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7388), 39, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7395) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Repellendus officiis magnam consequatur. Qui ut eveniet aut. Sunt aut occaecati quia optio deleniti aut amet in non.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7561), 29, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7568) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Placeat est eos praesentium aut optio officia. Unde odio porro. Corporis recusandae nulla ut dolores earum delectus. Quo vel error facere aut eum voluptatibus. Eligendi omnis tempore fugit est aliquam et.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7751), 29, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7757) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Magnam et perferendis aut.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7846), 35, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7853) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Omnis nostrum molestiae ipsum autem. Nulla consequatur neque. Aliquid ab porro. Ea assumenda assumenda. At ab quas et consequuntur illo illum. Inventore optio voluptate eum est dolores voluptatem qui.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8076), 40, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8082) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Animi voluptate veritatis aliquam alias. Architecto provident blanditiis. Sed delectus eum quia omnis placeat quia. Accusamus ex architecto doloribus. Sapiente sit suscipit numquam.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8236), 21, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8243) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "alias", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8511), 39, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Rerum odio quia quia delectus.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8664), 35, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8672) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Et iure perferendis voluptatem.
Accusamus labore voluptatibus qui dolore laborum quaerat aut et libero.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8794), 38, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8801) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Veniam optio odit et sed nemo rerum.
Et et necessitatibus.
Et omnis totam rerum sed voluptas.
Sit voluptas itaque.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8969), 23, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8976) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { @"Magnam qui explicabo eligendi rerum.
Voluptatum sint dolorem veniam.
Ratione tempore molestiae adipisci.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9074), 22, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9080) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 6, "Consectetur nisi blanditiis est.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9130), new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9136) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 10, 47, 24, 709, DateTimeKind.Local).AddTicks(1112), "Marge.Jacobson79@hotmail.com", "dZUDP17KvlBCYw1DPNTmIPRb2SVUA8d/BHKxuYoSwjg=", "wG5dtTlf1vtbhFjbd0InjOp58d5fokcbRV8sWq4rApw=", new DateTime(2020, 6, 9, 10, 47, 24, 709, DateTimeKind.Local).AddTicks(1738), "Tyreek49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 9, 10, 47, 24, 716, DateTimeKind.Local).AddTicks(3468), "Marietta_Paucek@gmail.com", "2VRUBKnnWOO3XOjWFkeQPOEyWVtTG34+cTM+M84KsOc=", "IccC7Lm0YPoe+aZZISIXlARjvFb9CKhGxJ2FqxWDyNg=", new DateTime(2020, 6, 9, 10, 47, 24, 716, DateTimeKind.Local).AddTicks(3565), "Elvera_Bernier39" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 9, 10, 47, 24, 723, DateTimeKind.Local).AddTicks(3701), "Zoie22@yahoo.com", "1fsbhUd06TyRmP4S4qXORn8pqqyUCtgWaaLU6fKPdHE=", "o9ri56eMArjs/6CbX/GOkz/Sein2b2RWGBx0meyzTcQ=", new DateTime(2020, 6, 9, 10, 47, 24, 723, DateTimeKind.Local).AddTicks(3775), "Forrest_Shields24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 10, 47, 24, 732, DateTimeKind.Local).AddTicks(1357), "Dudley76@yahoo.com", "A4xQLMNyuaAI4/xPCi+dQV9Z9MXSvkIFOpV9nYWVqsc=", "RPSLSnOGp63EmIdbm9iHt7hzLw+JWlHb2nKgEpRWKbY=", new DateTime(2020, 6, 9, 10, 47, 24, 732, DateTimeKind.Local).AddTicks(1443), "Melyssa.OKon" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 9, 10, 47, 24, 739, DateTimeKind.Local).AddTicks(719), "Brendan_Gutmann@hotmail.com", "cWW4PGnXn9AzJH1O/rSIsJ2Qcaqy8gFWi701NuYf0dY=", "7vWDK3wAptvcFvdwP2r5/Y7wXJvuGEeb4gRYTj4zYII=", new DateTime(2020, 6, 9, 10, 47, 24, 739, DateTimeKind.Local).AddTicks(767), "Joseph_Collins28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 9, 10, 47, 24, 745, DateTimeKind.Local).AddTicks(9844), "Sharon_Bailey@gmail.com", "IWgRlvbzAqNP1L38TVWTp8TEBJsSrRzX/CFX00F40TU=", "3Uon4xHMlEtQl1UcDJgM8xkjjo8pkyiXtEkKwlu5ECo=", new DateTime(2020, 6, 9, 10, 47, 24, 745, DateTimeKind.Local).AddTicks(9895), "Shanel_Crooks" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 9, 10, 47, 24, 753, DateTimeKind.Local).AddTicks(734), "Jane98@hotmail.com", "A3JrpvKOVB2lqqw0XiylFc9ZSeDA4ncnWnUJx2+KgUw=", "0Is6PBq1hDmFDmJcSSnS0Vv4ViQNmNcTqQiA5OADXd8=", new DateTime(2020, 6, 9, 10, 47, 24, 753, DateTimeKind.Local).AddTicks(792), "Nigel_Olson87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 9, 10, 47, 24, 759, DateTimeKind.Local).AddTicks(9638), "Cleta_Hoppe@gmail.com", "4wWzWTNJOSGhmP9ITIdp0nnRTqyj4GKZhJHSVwNb4iE=", "ZrXiCe42TtMhI49dGBC5U4USCWgOPcUb99ZWIh5wYW0=", new DateTime(2020, 6, 9, 10, 47, 24, 759, DateTimeKind.Local).AddTicks(9662), "Idella47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 9, 10, 47, 24, 766, DateTimeKind.Local).AddTicks(8370), "Sanford_Erdman16@hotmail.com", "dZYl+NRpKo9pnhli2miWe2ZiZS+rtoos98eYZnYHzlI=", "j7xukoVYWJc7nvgeoG/KZckk3RinaysNVseVdD8aH6g=", new DateTime(2020, 6, 9, 10, 47, 24, 766, DateTimeKind.Local).AddTicks(8389), "Glennie79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 10, 47, 24, 773, DateTimeKind.Local).AddTicks(6716), "Arlene_Durgan@hotmail.com", "imAT0g4b4jt8OUE2Fk4Q812qRMHzR+yvLbzrcroH5Bg=", "Ncms2T6vb+ZkQVrG9p+jhDsIHeNQytZLyAarj2OP42Q=", new DateTime(2020, 6, 9, 10, 47, 24, 773, DateTimeKind.Local).AddTicks(6746), "Antonietta.Hansen74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 9, 10, 47, 24, 780, DateTimeKind.Local).AddTicks(5888), "Peter8@yahoo.com", "Nbs+x7YkqWZLYkVWrZMaSrxBOzIXc8ck9RO8WYqlmj0=", "rEMDaIqNGqr8Mv7mb+KE+mgLlRnge5l/eD/xmLnfj3k=", new DateTime(2020, 6, 9, 10, 47, 24, 780, DateTimeKind.Local).AddTicks(5960), "Niko_Abshire" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 9, 10, 47, 24, 787, DateTimeKind.Local).AddTicks(4511), "Winston22@hotmail.com", "8fGRH8+4DQx9QVIpeemxlafPkbFsotOstUHEQlFJgBs=", "2QQaCBOEfBEMCyklUYTp5kg6w+gVvgBON+t+Qhn45SM=", new DateTime(2020, 6, 9, 10, 47, 24, 787, DateTimeKind.Local).AddTicks(4527), "Nolan_Shanahan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 9, 10, 47, 24, 794, DateTimeKind.Local).AddTicks(5791), "Gerardo76@hotmail.com", "a3Xoxs8zerVkXYStY5TcK1MTzXCGdgDRAAMZI+UJfpw=", "jnjIGvPth+A553tenTnV55zlrt+KtAgXF5U7L2lrafE=", new DateTime(2020, 6, 9, 10, 47, 24, 794, DateTimeKind.Local).AddTicks(5858), "Kali.Lockman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 9, 10, 47, 24, 801, DateTimeKind.Local).AddTicks(4578), "Sigmund78@yahoo.com", "iTS+mVVn+fSkirlqcMIgoNfWXNKuTctbPF1vfSytwmI=", "JR0RYpjVdaLbejTq5Vq0ReEqT/l22oAI8Ka9LHUjetg=", new DateTime(2020, 6, 9, 10, 47, 24, 801, DateTimeKind.Local).AddTicks(4602), "Dasia_Gorczany67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 9, 10, 47, 24, 808, DateTimeKind.Local).AddTicks(3072), "Emiliano80@gmail.com", "XFuEurLYlkZ1EAM5hNzQydpDc7vC5poH8PHebW7rAVY=", "ZG5PlwR/CLWac61bdYO1dGK++l40WqYdmRiTv1S+CXk=", new DateTime(2020, 6, 9, 10, 47, 24, 808, DateTimeKind.Local).AddTicks(3096), "Ross.Douglas57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 9, 10, 47, 24, 815, DateTimeKind.Local).AddTicks(1364), "Terrance.Glover59@hotmail.com", "7ji0jGgbFXakHnGw/fkloDqxNYgIEP5Azb8m3ANbvUA=", "od6A5Km7KVDWyrpC1P6vwI5v0GakrmXo057rC96zxZg=", new DateTime(2020, 6, 9, 10, 47, 24, 815, DateTimeKind.Local).AddTicks(1383), "Bernadine_Conn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 10, 47, 24, 821, DateTimeKind.Local).AddTicks(9570), "Marlen17@gmail.com", "ftu/5O6gln8A63EqFftOSyXD6B+TqfoNMrLUcBZkIRo=", "U/Uvf+fEu7ABaOiaW1z/Kr8jkFab/T/JDutJCee4G0U=", new DateTime(2020, 6, 9, 10, 47, 24, 821, DateTimeKind.Local).AddTicks(9632), "Chelsey_Nader" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 10, 47, 24, 828, DateTimeKind.Local).AddTicks(7799), "Patrick_Schamberger@hotmail.com", "csrCfvuhj1nUWVSSRpf8Zt6v6FjaVXxA4lX93uZCz40=", "NcJSxlMEmRQq+v8GSyANLwBPuklgHnFOvTJWP/M8Y9s=", new DateTime(2020, 6, 9, 10, 47, 24, 828, DateTimeKind.Local).AddTicks(7822), "Mireille83" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 9, 10, 47, 24, 835, DateTimeKind.Local).AddTicks(6229), "Nannie_Nicolas@yahoo.com", "p/zqpKxZyhXzUlRiEkhXWy1C2AEDEHS8wjIhoXfy63c=", "RSKl055WOfJD0oKcSciJHLWX5NQYcFUCpWlm0eI5A7k=", new DateTime(2020, 6, 9, 10, 47, 24, 835, DateTimeKind.Local).AddTicks(6258), "Drake_Boyle55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 9, 10, 47, 24, 842, DateTimeKind.Local).AddTicks(4957), "Carleton98@hotmail.com", "iiiAtgvxsTSN/J92thkn3sfU/JK/G7GgL0fZ76yW7M8=", "NBZCPHkScDEfsS/urHTbx3TpwIEwHc5tt+bg4elyBKg=", new DateTime(2020, 6, 9, 10, 47, 24, 842, DateTimeKind.Local).AddTicks(4994), "Ashlee_Spencer75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 849, DateTimeKind.Local).AddTicks(3120), "sQZ05gjLn48vR2odYqMsn8r2EfBLvjumxuwmfkv6yIE=", "qCuJMype13uWetAbTWrs3NHnXp8M3qkJb8GV4QAPpVw=", new DateTime(2020, 6, 9, 10, 47, 24, 849, DateTimeKind.Local).AddTicks(3120) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Posts");

            migrationBuilder.UpdateData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "IsLike", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1379), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1396) });

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 1, 11, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(6993), false, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(8315), 21 },
                    { 2, 18, new DateTime(2019, 6, 8, 13, 30, 1, 865, DateTimeKind.Local).AddTicks(9981), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(9), 6 },
                    { 3, 14, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(115), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(127), 18 },
                    { 4, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(208), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(224), 15 },
                    { 5, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(305), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(321), 2 },
                    { 6, 8, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(402), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(419), 14 },
                    { 7, 10, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(504), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(520), 12 },
                    { 8, 1, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(666), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(682), 19 },
                    { 9, 3, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(779), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(796), 16 },
                    { 20, 11, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2190), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2202), 2 },
                    { 11, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(978), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(994), 5 },
                    { 19, 2, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2101), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2113), 10 },
                    { 18, 7, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2008), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(2024), 18 },
                    { 17, 17, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1915), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1931), 1 },
                    { 10, 13, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(881), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(897), 15 },
                    { 14, 18, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1282), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1298), 7 },
                    { 13, 6, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1185), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1197), 1 },
                    { 12, 20, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1079), true, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1096), 9 },
                    { 16, 4, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1797), false, new DateTime(2019, 6, 8, 13, 30, 1, 866, DateTimeKind.Local).AddTicks(1821), 12 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Et minus amet.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(961), 3, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(2177) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 12, "Sit sint architecto.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4079), 13, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4119) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Ea ad deleniti ut quis officia voluptatibus occaecati.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4379), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4399) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "In eius qui necessitatibus et sapiente quis iure.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4626), 16, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4642) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 8, "Dolor doloremque est rerum et quis.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4837), 8, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(4853) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Tenetur occaecati omnis dolorem molestiae.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5039), 6, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5056) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Ut tempore ut id et odit temporibus.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5295), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5315) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Qui iste temporibus dolores.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5494), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5514) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Dolores ducimus magni qui nesciunt est quia aut a tempore.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5729), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5749) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Delectus non nihil cumque sed dolores ut impedit.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5952), 19, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(5972) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Consequuntur odit tempora omnis vel sunt illum nobis et quia.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6195), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6211) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "At aut veritatis veritatis.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6402), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6418) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 2, "Voluptates inventore libero sit illo pariatur.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6657), 1, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6677) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Quam magnam quidem cupiditate ratione id ab eum.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6904), 4, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(6925) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "At adipisci expedita dignissimos provident architecto.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7119), 17, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7135) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Architecto deleniti earum.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7285), 10, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7302) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Quasi iusto ipsa.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7460), 20, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7480) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut maiores eum a quibusdam non.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7837), 6, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(7861) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Soluta est animi.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8072), 12, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8092) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "In tempora et voluptatibus ut et consequatur fuga.", new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8287), 7, new DateTime(2019, 6, 8, 13, 30, 1, 827, DateTimeKind.Local).AddTicks(8303) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 425, DateTimeKind.Local).AddTicks(742), "https://s3.amazonaws.com/uifaces/faces/twitter/exentrich/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(2661) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(5920), "https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(5952) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6046), "https://s3.amazonaws.com/uifaces/faces/twitter/joshaustin/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6058) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6127), "https://s3.amazonaws.com/uifaces/faces/twitter/yecidsm/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6143) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6212), "https://s3.amazonaws.com/uifaces/faces/twitter/aoimedia/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6224) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6293), "https://s3.amazonaws.com/uifaces/faces/twitter/jonkspr/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6305) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6374), "https://s3.amazonaws.com/uifaces/faces/twitter/mutlu82/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6386) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6459), "https://s3.amazonaws.com/uifaces/faces/twitter/mgonto/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6471) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6540), "https://s3.amazonaws.com/uifaces/faces/twitter/karalek/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6552) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6621), "https://s3.amazonaws.com/uifaces/faces/twitter/igorgarybaldi/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6633) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6698), "https://s3.amazonaws.com/uifaces/faces/twitter/alek_djuric/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6715) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6779), "https://s3.amazonaws.com/uifaces/faces/twitter/renbyrd/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6796) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6860), "https://s3.amazonaws.com/uifaces/faces/twitter/mahmoudmetwally/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6873) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6942), "https://s3.amazonaws.com/uifaces/faces/twitter/mattdetails/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(6954) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7019), "https://s3.amazonaws.com/uifaces/faces/twitter/baumann_alex/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7100), "https://s3.amazonaws.com/uifaces/faces/twitter/kuldarkalvik/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7116) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7181), "https://s3.amazonaws.com/uifaces/faces/twitter/motionthinks/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7193) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7258), "https://s3.amazonaws.com/uifaces/faces/twitter/urrutimeoli/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7339), "https://s3.amazonaws.com/uifaces/faces/twitter/wesleytrankin/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7355) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7420), "https://s3.amazonaws.com/uifaces/faces/twitter/timmillwood/128.jpg", new DateTime(2019, 6, 8, 13, 30, 1, 426, DateTimeKind.Local).AddTicks(7436) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(539), "https://picsum.photos/640/480/?image=393", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(1638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2112), "https://picsum.photos/640/480/?image=1079", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2141) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2218), "https://picsum.photos/640/480/?image=801", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2234) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2299), "https://picsum.photos/640/480/?image=951", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2315) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2376), "https://picsum.photos/640/480/?image=436", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2392) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2457), "https://picsum.photos/640/480/?image=64", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2473) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2538), "https://picsum.photos/640/480/?image=436", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2639), "https://picsum.photos/640/480/?image=243", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2656) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2720), "https://picsum.photos/640/480/?image=515", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2733) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2797), "https://picsum.photos/640/480/?image=290", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2814) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2879), "https://picsum.photos/640/480/?image=400", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2891) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2956), "https://picsum.photos/640/480/?image=839", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(2972) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3037), "https://picsum.photos/640/480/?image=926", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3053) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3118), "https://picsum.photos/640/480/?image=163", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3130) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3195), "https://picsum.photos/640/480/?image=455", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3207) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3272), "https://picsum.photos/640/480/?image=688", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3284) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3349), "https://picsum.photos/640/480/?image=759", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3361) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3430), "https://picsum.photos/640/480/?image=307", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3442) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3507), "https://picsum.photos/640/480/?image=488", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3523) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3584), "https://picsum.photos/640/480/?image=902", new DateTime(2019, 6, 8, 13, 30, 1, 439, DateTimeKind.Local).AddTicks(3600) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 4, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3716), true, 6, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3728), 5 },
                    { 5, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3809), true, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3821), 16 },
                    { 6, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3906), false, 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3918), 13 },
                    { 2, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3452), false, 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3485), 13 },
                    { 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4097), true, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4113), 20 },
                    { 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4186), false, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4198), 5 },
                    { 10, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4275), false, 5, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4287), 15 },
                    { 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4004), true, 7, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4020), 18 },
                    { 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3606), false, 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(3623), 5 },
                    { 15, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4721), false, 11, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4733), 12 },
                    { 1, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(465), false, 2, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(1693), 1 },
                    { 14, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4632), true, 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4644), 20 },
                    { 11, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4364), false, 9, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4381), 8 },
                    { 16, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4806), true, 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4822), 16 },
                    { 17, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4904), false, 14, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4916), 15 },
                    { 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5001), true, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5017), 5 },
                    { 19, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5098), false, 3, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5114), 10 },
                    { 20, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5199), false, 8, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(5216), 19 },
                    { 13, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4543), false, 10, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4559), 16 },
                    { 12, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4458), false, 18, new DateTime(2019, 6, 8, 13, 30, 1, 848, DateTimeKind.Local).AddTicks(4470), 2 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Enim ea sunt eos reprehenderit maxime.", new DateTime(2019, 6, 8, 13, 30, 1, 808, DateTimeKind.Local).AddTicks(9179), 22, new DateTime(2019, 6, 8, 13, 30, 1, 809, DateTimeKind.Local).AddTicks(346) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Exercitationem assumenda expedita eos possimus dolorem aut repellat eos.
Error voluptate molestiae et laudantium.
Explicabo ea est totam soluta esse cumque voluptatibus qui voluptatem.
Ex corrupti praesentium omnis vitae occaecati.
Dicta doloribus qui vero sit incidunt id aspernatur ipsum.", new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(4781), 23, new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(4826) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Non placeat asperiores qui. Quis qui aut. Tempore iste dolores.", new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(9731), 34, new DateTime(2019, 6, 8, 13, 30, 1, 810, DateTimeKind.Local).AddTicks(9760) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, @"Atque rerum consequuntur illum rem quia labore.
Vel eos qui et sunt.
Officia aspernatur magnam molestiae.
Laudantium a voluptatibus ut expedita sed et ratione.
Cupiditate unde vitae officia quos.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(307), 31, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(323) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 10, "aperiam", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1361), 29, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1389) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Quisquam nihil natus quidem dolores iusto.
Qui et aspernatur est numquam harum et est totam et.
Accusantium itaque vel eum dolor asperiores placeat.
Aut voluptatum et.
Ut neque impedit voluptatum voluptatem vel aspernatur error nihil.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1941), 35, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(1957) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "sed", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2131), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2147) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 19, "Voluptas praesentium similique et voluptates unde illum libero necessitatibus facilis.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2387), 32, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2403) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Quos voluptas mollitia magni est quas.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2581), 24, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(2597) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Consequuntur ipsam est. Quo autem dolor rem quia quia perferendis et. Inventore laborum saepe. Quis dolorem provident cum a dolore voluptas et occaecati.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3027), 38, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3047) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Dolorum adipisci facere repellendus incidunt quia maiores. Pariatur iure esse nisi. Voluptatum eum error quasi repellat. Aliquid et tempore quo quis dolorem quasi. Aut consectetur itaque aut optio quis soluta qui corporis.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3530), 26, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3550) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Qui adipisci voluptatem ratione voluptatem laborum doloribus commodi.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3741), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(3757) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "In distinctio hic ex. Et neque dignissimos odit maiores repudiandae. Quisquam ut in vitae non minima.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4065), 36, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4081) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Aliquid eligendi aut omnis vitae in et ut. Voluptas provident aut sit. Hic ut et eius et quo. Voluptate minus ut facilis. Veniam dignissimos cum doloribus deserunt et.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4523), 38, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(4539) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Et vitae ipsa velit facere.
Est consectetur asperiores natus rerum culpa autem qui in.
Illum totam rerum a similique voluptates libero sed.
Iste neque ipsum quis odit doloremque.
Quis harum officia dolores sed sint blanditiis facilis dicta.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5058), 28, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5074) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Corporis nulla adipisci natus ab occaecati et nihil cumque. Velit et id adipisci. Deleniti ut necessitatibus provident autem nostrum maxime. Voluptatem deleniti corporis perferendis ullam officiis eius. Qui voluptatum mollitia aliquam voluptatem.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5545), 40, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5561) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "molestias", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5674), 24, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(5690) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 5, "Minima omnis quaerat ab est ducimus omnis voluptatem. Sunt iste nostrum non vitae placeat molestias ex eum. Saepe deserunt dignissimos qui earum minima. Quia fugiat voluptatem id quo illum sed.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6116), 31, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6132) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { @"Ut assumenda et et.
Voluptatem optio doloremque ipsa rerum nemo repellat quo.
Dignissimos ut magni fugiat quidem voluptatem est aut enim.
Ullam et eos quod ipsum reprehenderit rerum tempora possimus aut.
Porro reprehenderit explicabo illum sed et dolore.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6643), 30, new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6659) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "UpdatedAt" },
                values: new object[] { 16, @"Sapiente quis culpa velit commodi rerum iusto voluptatum neque.
Sunt sed eum nesciunt a vero.", new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6931), new DateTime(2019, 6, 8, 13, 30, 1, 811, DateTimeKind.Local).AddTicks(6943) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 8, 13, 30, 1, 498, DateTimeKind.Local).AddTicks(4064), "Lewis66@yahoo.com", "ipp+S75e/5LiuvKBM0nZrEQsP+7SMRMzqcaRN08Npeo=", "euuwrkqYzcRNJGdeN7pEtbfzLCvqO+eavPQlg+vBgT4=", new DateTime(2019, 6, 8, 13, 30, 1, 498, DateTimeKind.Local).AddTicks(5085), "Berta.Mann13" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 16, new DateTime(2019, 6, 8, 13, 30, 1, 510, DateTimeKind.Local).AddTicks(8861), "Zula.Schultz0@gmail.com", "P8ThNsvBpgNPvmH1YqSUD2/vtQRDH/Lul3MQa1elkRI=", "oI1mrCZQsTc837g5oIG3HMPEvL9DrfDpegteKB4mOas=", new DateTime(2019, 6, 8, 13, 30, 1, 510, DateTimeKind.Local).AddTicks(8946), "Emiliano_Rice44" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 8, 13, 30, 1, 523, DateTimeKind.Local).AddTicks(1692), "Reagan60@yahoo.com", "0seLIYhjNnliKhex2dDOLL/USB49B2vjmm35kPNClV0=", "2VgpQTnja97w2gvln6xsIeJnhGCvbckR1fahcmznjBw=", new DateTime(2019, 6, 8, 13, 30, 1, 523, DateTimeKind.Local).AddTicks(1773), "Keely_Johnston" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 535, DateTimeKind.Local).AddTicks(2230), "Keon.Lang62@yahoo.com", "6U7+IRUGC7b0o9gvk13Hmjq7fqhQrz5LUeBrI7N8KFc=", "yfVqUaYEqo8GpvMHeBTVWEvVt8tbqBlOg6RdI2UT4os=", new DateTime(2019, 6, 8, 13, 30, 1, 535, DateTimeKind.Local).AddTicks(2311), "Odell91" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 8, 13, 30, 1, 547, DateTimeKind.Local).AddTicks(4604), "Itzel.Hintz@yahoo.com", "NuSFkBsHxCG6xLWi69+t4D0QQGJ6BVVhb9xbXZLdvuw=", "RhFEqCkXU6TJ/PlnhiUc1oPhH+KaEWnNBGn83ls6cpI=", new DateTime(2019, 6, 8, 13, 30, 1, 547, DateTimeKind.Local).AddTicks(4685), "Elmore_Zieme78" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 12, new DateTime(2019, 6, 8, 13, 30, 1, 563, DateTimeKind.Local).AddTicks(166), "Lonie68@hotmail.com", "9kzThCD61VguYbsRHNtsljedbZNR1l25iA5h7tnglD4=", "urBn05zOW2W/z0SRh+ROKzQA8zstGURtGXA8s6Vb2a4=", new DateTime(2019, 6, 8, 13, 30, 1, 563, DateTimeKind.Local).AddTicks(255), "Alanis58" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 578, DateTimeKind.Local).AddTicks(2826), "Kristin97@gmail.com", "0V7MfOMBFz35VAxpeLP/VykoHmFr7Y6TMb7KmCsI/aY=", "7sMIRbE3J8XKIl6X9jKvzdPbJKIBm2ObprWHVvqbjCg=", new DateTime(2019, 6, 8, 13, 30, 1, 578, DateTimeKind.Local).AddTicks(3337), "Mallory.Lowe" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 8, 13, 30, 1, 591, DateTimeKind.Local).AddTicks(1073), "Brock.Morissette45@yahoo.com", "W0C3ON2PXLVca3hpSsshp9A731PcJZ/uhs6iqIEPNO0=", "rfSm1BxD/Cz3A0fKq9SM3lViajI8Rf0JUblIsycGTHk=", new DateTime(2019, 6, 8, 13, 30, 1, 591, DateTimeKind.Local).AddTicks(1166), "Adrian47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2019, 6, 8, 13, 30, 1, 603, DateTimeKind.Local).AddTicks(309), "Chester.Botsford18@hotmail.com", "TD3rl13TivQ0isdJJdv3otUiNVS1qu95yZQACMf2xlw=", "D+kG+ztr9Id+AD59gxJdJygPRlt/ioTnMqMBlmFDyjE=", new DateTime(2019, 6, 8, 13, 30, 1, 603, DateTimeKind.Local).AddTicks(394), "Alysha_Kovacek" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2019, 6, 8, 13, 30, 1, 617, DateTimeKind.Local).AddTicks(9451), "Marcelina62@yahoo.com", "aLjZF/bDcOOeW7qCP9dwuSPumeOm9QRufFkUKft46+4=", "bh5Ft5t/KbW9P4fmQXyGpy+XgKou4wo7+Rdd1ySJm8Y=", new DateTime(2019, 6, 8, 13, 30, 1, 617, DateTimeKind.Local).AddTicks(9552), "Rupert_Kassulke55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 639, DateTimeKind.Local).AddTicks(2043), "Shaylee79@yahoo.com", "Rh0T1eDmyKZIDhDBfPV7+xzjf1K+T84PENeigQVqr+E=", "bYVeC1RbOLLbVVmDf1eTE1dAGCKYfgEgYbNWtgYNIyk=", new DateTime(2019, 6, 8, 13, 30, 1, 639, DateTimeKind.Local).AddTicks(2132), "Gwendolyn_Boehm" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 661, DateTimeKind.Local).AddTicks(877), "Mauricio.Von33@yahoo.com", "UTni2loimGZ5Kti93HaEZ+fciZCwfO/ZaPOc44RxhhM=", "cYSMaR0B14+YVoFvlt8tjpPoZY2z5K5jIFb2xsHfxZs=", new DateTime(2019, 6, 8, 13, 30, 1, 661, DateTimeKind.Local).AddTicks(966), "Coleman_Bosco95" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 8, 13, 30, 1, 673, DateTimeKind.Local).AddTicks(2489), "Elton.Farrell22@hotmail.com", "DSLPWWQIc4TRCSD6QevyvmOV2Q8UikaYj+CawmvEYhM=", "Nd3AGdFKVWNX6lV8EepGfNXRRTrIbZt+crUIxwN7K4A=", new DateTime(2019, 6, 8, 13, 30, 1, 673, DateTimeKind.Local).AddTicks(2582), "Delores_Hauck92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2019, 6, 8, 13, 30, 1, 685, DateTimeKind.Local).AddTicks(5677), "Sabryna13@hotmail.com", "2I1TP1TVsZiysC0zeVLdGzmL6bsTG5Yro1ZMoZPOVZs=", "2ttqWS2Z+EZN+C1+MQpRFtwsg1kh/bqEJOhp5GaQdo0=", new DateTime(2019, 6, 8, 13, 30, 1, 685, DateTimeKind.Local).AddTicks(5778), "Lucie32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2019, 6, 8, 13, 30, 1, 702, DateTimeKind.Local).AddTicks(7746), "Ardith.Cummings95@yahoo.com", "XKm4g0rS/kMzXDlq1A0xd0Mj1UTKz78QZqEIjGiVzaI=", "joVmnahae87VaRCkiCZANdQn93d2cMmII5sssUek7AQ=", new DateTime(2019, 6, 8, 13, 30, 1, 702, DateTimeKind.Local).AddTicks(7844), "Benton99" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2019, 6, 8, 13, 30, 1, 724, DateTimeKind.Local).AddTicks(7786), "Wilfrid54@yahoo.com", "dymXvZziyvE784wDCRRuv+Kazm4duf2yUAZQJG9MFzw=", "6iWfzQsuyR5S7/Xx02k+yJV5y6UWvnNU4rUmZYTba58=", new DateTime(2019, 6, 8, 13, 30, 1, 724, DateTimeKind.Local).AddTicks(7875), "Shaylee.King" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 4, new DateTime(2019, 6, 8, 13, 30, 1, 741, DateTimeKind.Local).AddTicks(6309), "Harold.Rowe71@yahoo.com", "LoU3WAtUBJTUjad+UJqki5rZ6n/M9omgb/3+yVk7vMw=", "n/uUFtMkyUefMnQNz/j6Vaqzr/osn7pBREHzWM8lIU8=", new DateTime(2019, 6, 8, 13, 30, 1, 741, DateTimeKind.Local).AddTicks(6406), "Cali.Waters" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2019, 6, 8, 13, 30, 1, 753, DateTimeKind.Local).AddTicks(8333), "Samanta_OReilly95@yahoo.com", "+YA8dDK1ZoLFTvfpnVQGzmhKXHRaAgN/fQjmZMVMGcM=", "Bhn9S4MMVApZjnGJUamRVV4ikTzCgdc/KBbTMldMsjY=", new DateTime(2019, 6, 8, 13, 30, 1, 753, DateTimeKind.Local).AddTicks(8426), "Baylee_Sipes42" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2019, 6, 8, 13, 30, 1, 765, DateTimeKind.Local).AddTicks(8563), "Rex64@hotmail.com", "JOICOM6OozRPh4VLEbxS/iJMsI1HhHyOdDtuG4tau78=", "EdoXXUo+awEKlbwKaH/IFKIzz1Ybyl69YYeYJACQ/nM=", new DateTime(2019, 6, 8, 13, 30, 1, 765, DateTimeKind.Local).AddTicks(8648), "Sharon32" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2019, 6, 8, 13, 30, 1, 779, DateTimeKind.Local).AddTicks(1504), "Brice_Abshire72@gmail.com", "D+HiHDuS3LIsqdZUuSEQeZ0VSpvsWiTYTKiW1JwGMPU=", "unJ1mBlcdh5pmmYITwm9s49RsQknqWgoUYsqdrOvjbI=", new DateTime(2019, 6, 8, 13, 30, 1, 779, DateTimeKind.Local).AddTicks(1590), "Kayla46" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2019, 6, 8, 13, 30, 1, 791, DateTimeKind.Local).AddTicks(1782), "i2TJ0z00NYlgYE1RckEz65RuduHNOi6XLzkY/zjls3M=", "OfXWhBQuNh3ZiHzV/LYq5X7zvFdI56jXiAJ/cQcbvJM=", new DateTime(2019, 6, 8, 13, 30, 1, 791, DateTimeKind.Local).AddTicks(1782) });
        }
    }
}
