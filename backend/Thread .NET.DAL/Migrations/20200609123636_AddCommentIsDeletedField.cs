﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Thread_.NET.DAL.Migrations
{
    public partial class AddCommentIsDeletedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Comments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 13, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7184), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7188), 21 },
                    { 2, 14, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6457), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6475), 20 },
                    { 3, 8, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6510), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6516), 6 },
                    { 4, 1, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6555), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6560), 8 },
                    { 5, 14, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6583), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6588), 14 },
                    { 6, 12, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6611), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6616), 10 },
                    { 7, 8, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6639), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6644), 9 },
                    { 8, 14, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6665), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6670), 7 },
                    { 9, 5, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6692), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6697), 7 },
                    { 10, 5, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6717), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6722), 10 },
                    { 1, 13, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(5342), true, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(5930), 13 },
                    { 12, 15, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6769), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6774), 9 },
                    { 13, 5, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6796), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6801), 3 },
                    { 14, 8, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6822), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6827), 3 },
                    { 15, 4, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6851), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6857), 1 },
                    { 16, 3, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6894), true, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6929), 7 },
                    { 17, 7, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7079), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7095), 19 },
                    { 18, 18, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7133), false, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7138), 19 },
                    { 19, 7, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7159), true, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(7163), 7 },
                    { 11, 7, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6744), true, new DateTime(2020, 6, 9, 15, 36, 35, 529, DateTimeKind.Local).AddTicks(6749), 18 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Nam similique maiores.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(1818), 16, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(2338) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Minus eos eum.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(2982), 13, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3003) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Blanditiis ea qui.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3160), 4, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3168) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Aut provident vel.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3219), 6, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3225) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Accusantium dolorum inventore quos et aut.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3286), 14, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3292) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Excepturi at qui pariatur.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3339), 15, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3345) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Hic ea in aliquam asperiores dolor.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3402), 12, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3408) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Ex pariatur temporibus.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3450), 20, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3456) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Ut nemo sunt est qui illo et architecto.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3519), 14, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3525) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Et earum unde alias neque et corporis reprehenderit culpa aut.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3643), 1, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3650) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 18, "Pariatur minus laudantium qui quo quia.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3704), 4, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3709) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Omnis tenetur minima minima.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3769), 12, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3800) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 6, "Nihil voluptas quisquam error voluptatem.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(3992), 19, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4010) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Explicabo consectetur aliquid.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4078), 2, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4084) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Omnis porro ullam ratione molestiae velit.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4141), 17, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4146) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 13, "Qui vero et quo dolorem.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4280), 15, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4302) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 9, "Laborum impedit doloremque officiis est laudantium dignissimos ea deserunt temporibus.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4483), 7, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4523) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Est nisi beatae eum sit labore reiciendis.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4655), 20, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4663) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 15, "Assumenda vero delectus doloribus.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4718), 5, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4724) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 11, "Omnis ducimus quis corporis id omnis inventore.", new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4787), 3, new DateTime(2020, 6, 9, 15, 36, 35, 516, DateTimeKind.Local).AddTicks(4794) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(2995), "https://s3.amazonaws.com/uifaces/faces/twitter/karlkanall/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(6491) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7217), "https://s3.amazonaws.com/uifaces/faces/twitter/peterlandt/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7242) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7276), "https://s3.amazonaws.com/uifaces/faces/twitter/chrismj83/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7303), "https://s3.amazonaws.com/uifaces/faces/twitter/dhrubo/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7308) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7328), "https://s3.amazonaws.com/uifaces/faces/twitter/ChrisFarina78/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7333) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7353), "https://s3.amazonaws.com/uifaces/faces/twitter/benefritz/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7358) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7378), "https://s3.amazonaws.com/uifaces/faces/twitter/hgharrygo/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7383) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7403), "https://s3.amazonaws.com/uifaces/faces/twitter/el_fuertisimo/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7408) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7428), "https://s3.amazonaws.com/uifaces/faces/twitter/bryan_topham/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7433) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7451), "https://s3.amazonaws.com/uifaces/faces/twitter/clubb3rry/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7456) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7475), "https://s3.amazonaws.com/uifaces/faces/twitter/dotgridline/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7479) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7499), "https://s3.amazonaws.com/uifaces/faces/twitter/umurgdk/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7504) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7522), "https://s3.amazonaws.com/uifaces/faces/twitter/nicoleglynn/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7527) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7546), "https://s3.amazonaws.com/uifaces/faces/twitter/brandonmorreale/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7550) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7567), "https://s3.amazonaws.com/uifaces/faces/twitter/benefritz/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7572) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7592), "https://s3.amazonaws.com/uifaces/faces/twitter/olaolusoga/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7597) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7616), "https://s3.amazonaws.com/uifaces/faces/twitter/msveet/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7620) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7640), "https://s3.amazonaws.com/uifaces/faces/twitter/guischmitt/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7645) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7671), "https://s3.amazonaws.com/uifaces/faces/twitter/olaolusoga/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7679) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7781), "https://s3.amazonaws.com/uifaces/faces/twitter/ninjad3m0/128.jpg", new DateTime(2020, 6, 9, 15, 36, 35, 324, DateTimeKind.Local).AddTicks(7805) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(8042), "https://picsum.photos/640/480/?image=361", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(8594) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(8887), "https://picsum.photos/640/480/?image=112", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(8982) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9060), "https://picsum.photos/640/480/?image=8", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9066) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9088), "https://picsum.photos/640/480/?image=595", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9093) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9111), "https://picsum.photos/640/480/?image=419", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9116) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9134), "https://picsum.photos/640/480/?image=193", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9139) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9157), "https://picsum.photos/640/480/?image=70", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9162) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9180), "https://picsum.photos/640/480/?image=582", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9185) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9203), "https://picsum.photos/640/480/?image=382", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9208) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9225), "https://picsum.photos/640/480/?image=128", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9230) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9247), "https://picsum.photos/640/480/?image=961", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9251) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9270), "https://picsum.photos/640/480/?image=1057", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9274) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9292), "https://picsum.photos/640/480/?image=610", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9297) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9314), "https://picsum.photos/640/480/?image=568", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9319) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9421), "https://picsum.photos/640/480/?image=820", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9426) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9445), "https://picsum.photos/640/480/?image=460", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9450) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9467), "https://picsum.photos/640/480/?image=368", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9471) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9489), "https://picsum.photos/640/480/?image=445", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9494) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9511), "https://picsum.photos/640/480/?image=727", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9516) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9533), "https://picsum.photos/640/480/?image=574", new DateTime(2020, 6, 9, 15, 36, 35, 329, DateTimeKind.Local).AddTicks(9538) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5270), false, 1, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5275), 5 },
                    { 11, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5297), false, 7, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5302), 1 },
                    { 12, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5323), false, 3, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5328), 21 },
                    { 13, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5349), true, 12, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5354), 14 },
                    { 15, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5402), true, 10, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5406), 4 },
                    { 18, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5478), true, 10, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5483), 17 },
                    { 16, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5427), false, 2, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5432), 14 },
                    { 17, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5453), false, 2, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5457), 12 },
                    { 9, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5242), true, 20, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5247), 1 },
                    { 20, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5529), false, 13, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5533), 7 },
                    { 19, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5503), true, 3, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5508), 17 },
                    { 14, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5375), false, 3, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5380), 7 },
                    { 8, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5216), true, 4, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5221), 16 },
                    { 3, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4920), false, 20, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4926), 18 },
                    { 6, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5156), false, 11, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5161), 14 },
                    { 5, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5100), true, 15, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5117), 8 },
                    { 1, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(3735), true, 15, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4353), 18 },
                    { 4, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4963), false, 18, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4971), 16 },
                    { 2, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4868), true, 4, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(4886), 14 },
                    { 7, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5190), true, 14, new DateTime(2020, 6, 9, 15, 36, 35, 523, DateTimeKind.Local).AddTicks(5195), 1 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, @"Voluptatem est voluptatem eos non ut ipsam.
Dignissimos et occaecati est ea minus magnam aliquid.
Et itaque aut eveniet.
Cumque unde inventore ab qui vero dignissimos quasi est.
Minima doloremque consequatur.", new DateTime(2020, 6, 9, 15, 36, 35, 509, DateTimeKind.Local).AddTicks(9001), 28, new DateTime(2020, 6, 9, 15, 36, 35, 509, DateTimeKind.Local).AddTicks(9609) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Dolores dignissimos nisi voluptatem est.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(744), 34, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(772) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "et", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1074), 37, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1087) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "vel", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1130), 32, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1136) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, @"Est et nesciunt sunt praesentium eius quis debitis velit ducimus.
Commodi et eos et aut.
Distinctio omnis blanditiis illo accusamus impedit perferendis est illo.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1457), 27, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1465) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Optio aliquid doloribus deleniti porro natus sed.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1564), 21, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1571) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "rerum", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1605), 36, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(1610) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Itaque enim dolores et est et. Molestiae at et sunt. Odio ea et nemo. Rerum placeat voluptatem quis iure ratione. Cum omnis cum molestiae molestias.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(2620), 26, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(2637) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, "Sed ipsa recusandae eveniet. Est tempora et fuga consequatur totam inventore id culpa. Rerum quis eum sequi. Et unde quia eum qui perferendis dolore optio.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(2802), 29, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(2809) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Temporibus incidunt quis eos quis.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(2897), 32, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(2904) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 2, "Omnis suscipit quo dolorum sequi. Quia accusamus asperiores. Est quo qui sunt. Aut architecto quod non non est inventore molestias sit enim. Nostrum ea ut cumque magnam dolor. Possimus repellendus repellendus doloribus velit vero ratione quos perspiciatis.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3148), 32, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3154) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, @"Quo earum enim.
Iure ducimus voluptatem expedita nihil eum expedita.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3259), 28, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3265) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, "Exercitationem delectus rem et eum temporibus dolores. Officiis aut vel. Aut atque nihil. Adipisci quis culpa quam repellat amet et. Qui aut beatae a suscipit pariatur beatae nisi. Earum minima fugit neque quia cumque molestiae aut.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3499), 24, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3505) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, @"Iste modi ea odit.
Quos tempora molestiae est.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3581), 23, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3587) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Impedit quia omnis voluptatem et.
Reprehenderit neque sed.
Eum maiores officia et laborum.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3767), 37, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3774) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 8, "Ex perspiciatis vero iste tempora optio officia fugit.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3835), 25, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3841) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 20, "perferendis", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3872), 29, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3877) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Et eum laborum nihil.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3927), 21, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3932) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 13, "Explicabo est ipsum.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3977), 39, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(3983) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 11, "Maxime vitae molestiae id pariatur dolorum unde. Numquam et ipsum dolorem doloremque. Sapiente inventore ipsum iusto maiores. Itaque expedita et occaecati voluptatem quo. Quas cupiditate nobis perspiciatis voluptate error. Ut laboriosam alias soluta consequatur aut fugiat.", new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(4211), 26, new DateTime(2020, 6, 9, 15, 36, 35, 510, DateTimeKind.Local).AddTicks(4218) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 363, DateTimeKind.Local).AddTicks(9058), "Virgil96@hotmail.com", "/rA5brs228r+YKIE/2RCTOQzLLk+xiUf+9j8YHcxu38=", "x8zbBVeziCdeaqL+6+lEr/4EgOy8DOE0InQcTe0SrfM=", new DateTime(2020, 6, 9, 15, 36, 35, 363, DateTimeKind.Local).AddTicks(9603), "Aliyah92" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 5, new DateTime(2020, 6, 9, 15, 36, 35, 371, DateTimeKind.Local).AddTicks(81), "August.Bogan@hotmail.com", "8kAn3MOtZ5DLYaJplVBoHIjMVcNEVHo6Ph7J1La+DaM=", "/k3L7PucdhuK9wkICJ7IrIsVknYOjIiAvYOeYxyE0g4=", new DateTime(2020, 6, 9, 15, 36, 35, 371, DateTimeKind.Local).AddTicks(113), "Birdie37" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 15, 36, 35, 377, DateTimeKind.Local).AddTicks(8584), "Alberto_Gleichner@hotmail.com", "8hLg3LwdWZfbLiKt1S0UwESPkIfffJTRoXmYcAO5r9I=", "C9I34TcYUAkf05nqipI7dTBFIz7YuWIrdcHyURE7NBM=", new DateTime(2020, 6, 9, 15, 36, 35, 377, DateTimeKind.Local).AddTicks(8596), "Haley_Larson" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 15, 36, 35, 384, DateTimeKind.Local).AddTicks(6914), "Arvilla89@yahoo.com", "QqqV9709trDTuRq3NGlrSzD8yGyMjZ6s9RjkkaR/5kk=", "RMmBWcma5P5+jTRmy/lIHg0R5sUmfesy0z7/ONmQCQ0=", new DateTime(2020, 6, 9, 15, 36, 35, 384, DateTimeKind.Local).AddTicks(6924), "Lilliana_Weissnat" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 9, 15, 36, 35, 391, DateTimeKind.Local).AddTicks(5214), "Floy62@hotmail.com", "j7pb6nxLwmJcsw86xMv9pJ+hD9wBwVufEKtyyKTpvL0=", "/i2iZHIb2b6W8vHewh/y+Py8tEYhXlLi8JIQOUogyCo=", new DateTime(2020, 6, 9, 15, 36, 35, 391, DateTimeKind.Local).AddTicks(5223), "Adam_Funk0" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 15, 36, 35, 398, DateTimeKind.Local).AddTicks(3774), "Ericka20@yahoo.com", "M5/j1AdN4ZBWAK3DhkPvvvd8D0BT6eLX6OMYB04wbbo=", "01eWLM18QDyIxab+Vxkz26h9MQWKsP+9v4u6Zr1Jixc=", new DateTime(2020, 6, 9, 15, 36, 35, 398, DateTimeKind.Local).AddTicks(3785), "Keshawn.Douglas" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 9, 15, 36, 35, 405, DateTimeKind.Local).AddTicks(2138), "Stacy_Prohaska@hotmail.com", "UKQ/xl4MXxB4HT247kYBM6zr7Rl/DlG3In9u3xOWQUs=", "ACUEACXcImFC4Rh3vMTstzdwCdgYxnAGqKAJUXEOWZo=", new DateTime(2020, 6, 9, 15, 36, 35, 405, DateTimeKind.Local).AddTicks(2147), "Antwan_Orn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 15, 36, 35, 412, DateTimeKind.Local).AddTicks(479), "Lizeth.Reichert@yahoo.com", "zea3NJP8Mb0poG+nO++1jHFbRrLnPHCndfYc1mry0aA=", "Qq7DYeTPfWkU+WtI9/tNdzcLeBOEBUt44JkRfDD/moI=", new DateTime(2020, 6, 9, 15, 36, 35, 412, DateTimeKind.Local).AddTicks(489), "Allison.Sauer" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 9, 15, 36, 35, 418, DateTimeKind.Local).AddTicks(8822), "Krystal_Nikolaus@yahoo.com", "wUJIfPWeAR8UloJez4u2vUjoY0c0H+vD9mDZ67zVyww=", "VU+oW9LyT4TmspQgY3cdTI1PcXwnT81MfMnDE2vpot4=", new DateTime(2020, 6, 9, 15, 36, 35, 418, DateTimeKind.Local).AddTicks(8831), "Sandy28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 9, 15, 36, 35, 425, DateTimeKind.Local).AddTicks(7358), "Candice.Pollich@gmail.com", "QengcBT/rust4RscMav9SLT7ljOQfbDNQIJjVfR1akw=", "aBcGHufw9raFQ9YyFC9qE0eNKNObkC9GgTJJTfuRJhk=", new DateTime(2020, 6, 9, 15, 36, 35, 425, DateTimeKind.Local).AddTicks(7370), "Ivory.Kassulke" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 9, 15, 36, 35, 432, DateTimeKind.Local).AddTicks(5726), "Krystel.Macejkovic38@yahoo.com", "6qno/lOmI/gMcieq6EMcfSnKi2ah9I+zCdC6psXLr6c=", "mpxMmMCkRrnMyRJziNYKqAlPIRKYL01v9ixCCDfWNRc=", new DateTime(2020, 6, 9, 15, 36, 35, 432, DateTimeKind.Local).AddTicks(5735), "Eloy2" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 9, 15, 36, 35, 439, DateTimeKind.Local).AddTicks(4007), "Mazie.Huel@hotmail.com", "Pnu2cOJPjByrNJI3tMWiJKjACaJ97xG7XPXO6uZeCrc=", "v+DJxk3F2o7AXmPHI2RclxeQUEteiCGDU13OaEemG4Q=", new DateTime(2020, 6, 9, 15, 36, 35, 439, DateTimeKind.Local).AddTicks(4016), "Herbert52" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 9, 15, 36, 35, 446, DateTimeKind.Local).AddTicks(2355), "Fabiola.Schuster@gmail.com", "FJ7Uo0MLmCwA0yWtH6tObcs3m3lrkLwJyCqW37VspQk=", "uA4z5y8ze738L2gNA3nuHsDXzIOGqL8Gmoda5eicH+I=", new DateTime(2020, 6, 9, 15, 36, 35, 446, DateTimeKind.Local).AddTicks(2363), "Jayda89" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 15, 36, 35, 453, DateTimeKind.Local).AddTicks(649), "Greg20@yahoo.com", "V1NoVT4P5I8X5CLZUTy8qhiJi60wxDyuVetvqbKHoL0=", "fXmuFq8enANfHK8bmA7TkUBADa9lVUj5/TiY/SsLKEk=", new DateTime(2020, 6, 9, 15, 36, 35, 453, DateTimeKind.Local).AddTicks(659), "Leatha15" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 15, 36, 35, 459, DateTimeKind.Local).AddTicks(9112), "Brandyn66@hotmail.com", "fzBTKS8bH6DPJbkAQzHzODibsxLGU9Rv8JVo3DRH1b4=", "zklmlEkHDN1GuXtxAuKvGfmJpGdRRbN9jk018+swo4o=", new DateTime(2020, 6, 9, 15, 36, 35, 459, DateTimeKind.Local).AddTicks(9122), "Sam_Blanda56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 3, new DateTime(2020, 6, 9, 15, 36, 35, 466, DateTimeKind.Local).AddTicks(7414), "Dawn_Halvorson@yahoo.com", "BrsSGrE9CfXv9Wnx382Df+XqK4B0gOWLddqtt9rNdRw=", "6CZrJl9wcLzYcEz+bDHYQH1jRjUUs/6sJR+5qcZZd48=", new DateTime(2020, 6, 9, 15, 36, 35, 466, DateTimeKind.Local).AddTicks(7425), "Kole.Hauck1" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 2, new DateTime(2020, 6, 9, 15, 36, 35, 473, DateTimeKind.Local).AddTicks(5743), "Willie.Raynor90@yahoo.com", "b2HWFPyhkkJUBMy1LaFFd4uAE09zNmeVHEPXj6n554s=", "XDFIejZUrfe4k+kpIlCnh5mQesHogvtpfzCuK5neMbE=", new DateTime(2020, 6, 9, 15, 36, 35, 473, DateTimeKind.Local).AddTicks(5752), "Margie_Hayes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 15, 36, 35, 480, DateTimeKind.Local).AddTicks(4013), "Reyna.Rosenbaum@hotmail.com", "bzr0xOMgle3cC1jJnG6KJv4E+XPkXXCSgNjBxeq19ig=", "q3880S+hN7NE3Sw0lZDulvDPCihAJt+ySYoN9OPegS8=", new DateTime(2020, 6, 9, 15, 36, 35, 480, DateTimeKind.Local).AddTicks(4022), "Sydnee.Welch56" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 9, 15, 36, 35, 487, DateTimeKind.Local).AddTicks(2442), "Jay.Paucek@yahoo.com", "z55TiOwBO1JkgPJ+S/I67upLLguJ4g5vi/IOJoawug0=", "8N18l1T2MV8SfnwNx6VujMgbjuKvdRfNeceUkU+3vhc=", new DateTime(2020, 6, 9, 15, 36, 35, 487, DateTimeKind.Local).AddTicks(2451), "Lucas.Padberg" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 9, 15, 36, 35, 494, DateTimeKind.Local).AddTicks(1143), "Tanner39@hotmail.com", "OUGYNUyKAVHhh68E0XdaMtN0oi9c2KIWYtwKw2Ida7I=", "G1KkR1aMMC5Hqy5GT2f/FDyCZMkdLU2IzB53XhNf5lk=", new DateTime(2020, 6, 9, 15, 36, 35, 494, DateTimeKind.Local).AddTicks(1456), "Juvenal.Abernathy49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 15, 36, 35, 501, DateTimeKind.Local).AddTicks(253), "KOHbNxBrY1/aPXiynd+WHFaf5LxXHKbmtSgQ0mlh8YU=", "cXiHyCsz5a5ix9QCj5PaYJZJAspvsz+q1v3KAmg11wk=", new DateTime(2020, 6, 9, 15, 36, 35, 501, DateTimeKind.Local).AddTicks(253) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "CommentReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "PostReactions",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Comments");

            migrationBuilder.InsertData(
                table: "CommentReactions",
                columns: new[] { "Id", "CommentId", "CreatedAt", "IsLike", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 20, 9, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8067), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8072), 7 },
                    { 2, 13, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7574), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7591), 11 },
                    { 3, 2, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7625), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7630), 13 },
                    { 4, 2, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7653), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7658), 4 },
                    { 5, 5, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7679), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7684), 18 },
                    { 6, 6, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7709), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7714), 1 },
                    { 7, 6, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7734), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7738), 18 },
                    { 8, 18, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7760), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7764), 14 },
                    { 9, 16, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7786), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7791), 2 },
                    { 10, 4, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7812), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7816), 12 },
                    { 1, 1, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(6667), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7115), 18 },
                    { 12, 13, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7863), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7867), 6 },
                    { 13, 15, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7889), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7893), 19 },
                    { 14, 10, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7915), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7919), 3 },
                    { 15, 20, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7941), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7945), 13 },
                    { 16, 17, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7967), false, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7971), 15 },
                    { 17, 4, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7991), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7996), 6 },
                    { 18, 5, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8017), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8021), 20 },
                    { 19, 4, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8042), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(8046), 21 },
                    { 11, 17, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7837), true, new DateTime(2020, 6, 9, 10, 47, 24, 875, DateTimeKind.Local).AddTicks(7842), 14 }
                });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Modi nulla nisi sunt beatae modi minus.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(6515), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(6945) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Consequatur et magnam adipisci deserunt voluptas ullam.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7509), 2, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7527) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 5, "Suscipit aliquam similique eos mollitia tempore nihil.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7608), 20, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7615) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 20, "Qui quam velit odit ut harum sunt mollitia sed.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7740), 9, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7746) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Incidunt est ut id odit accusantium animi et.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7812), 2, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7818) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 17, "Distinctio velit delectus ad atque aut voluptate.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7878), 11, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7884) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Eveniet vero iure excepturi ratione modi a ut cumque praesentium.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7953), 5, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(7959) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Accusantium a mollitia asperiores error in deserunt dolores.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8020), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8025) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Placeat et alias ex sunt fuga quibusdam quia officiis.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8144), 7, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8150) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 10, "Debitis explicabo sapiente et rerum ut blanditiis mollitia atque.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8217), 12, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8223) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Ipsam rerum voluptatibus voluptas ea rerum voluptatum quaerat amet qui.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8293), 5, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8299) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 1, "Animi quia beatae nam.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8347), 7, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8352) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 7, "Eligendi quia porro eveniet id animi hic voluptatem sit architecto.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8420), 6, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8426) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 16, "Veritatis ipsa sed facilis tenetur.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8520), 19, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8526) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { "Architecto similique exercitationem suscipit quisquam.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8590), 1, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8597) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 3, "Autem recusandae voluptatum.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8641), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8646) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 21, "Aut ipsum vitae labore et.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8701), 5, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8706) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 19, "Fugit voluptatem laudantium.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8747), 4, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8752) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 14, "Ut iusto quo nostrum voluptate est dolorum.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8810), 9, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8815) });

            migrationBuilder.UpdateData(
                table: "Comments",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PostId", "UpdatedAt" },
                values: new object[] { 4, "Rerum voluptatem repudiandae sed consequatur eos nulla.", new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8869), 14, new DateTime(2020, 6, 9, 10, 47, 24, 862, DateTimeKind.Local).AddTicks(8875) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(3822), "https://s3.amazonaws.com/uifaces/faces/twitter/heycamtaylor/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7186) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7830), "https://s3.amazonaws.com/uifaces/faces/twitter/arashmanteghi/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7852) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7928), "https://s3.amazonaws.com/uifaces/faces/twitter/sprayaga/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7933) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7958), "https://s3.amazonaws.com/uifaces/faces/twitter/josemarques/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7962) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7982), "https://s3.amazonaws.com/uifaces/faces/twitter/thatonetommy/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(7987) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8007), "https://s3.amazonaws.com/uifaces/faces/twitter/tanveerrao/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8011) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8030), "https://s3.amazonaws.com/uifaces/faces/twitter/peejfancher/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8035) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8054), "https://s3.amazonaws.com/uifaces/faces/twitter/scrapdnb/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8058) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8080), "https://s3.amazonaws.com/uifaces/faces/twitter/mrebay007/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8085) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8104), "https://s3.amazonaws.com/uifaces/faces/twitter/nicklacke/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8109) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8127), "https://s3.amazonaws.com/uifaces/faces/twitter/ceekaytweet/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8132) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8151), "https://s3.amazonaws.com/uifaces/faces/twitter/mrxloka/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8156) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8174), "https://s3.amazonaws.com/uifaces/faces/twitter/eyronn/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8179) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8196), "https://s3.amazonaws.com/uifaces/faces/twitter/joelhelin/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8201) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8219), "https://s3.amazonaws.com/uifaces/faces/twitter/bertboerland/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8223) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8243), "https://s3.amazonaws.com/uifaces/faces/twitter/suprb/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8247) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8265), "https://s3.amazonaws.com/uifaces/faces/twitter/d33pthought/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8270) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8289), "https://s3.amazonaws.com/uifaces/faces/twitter/artem_kostenko/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8294) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8313), "https://s3.amazonaws.com/uifaces/faces/twitter/attacks/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8317) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8335), "https://s3.amazonaws.com/uifaces/faces/twitter/croakx/128.jpg", new DateTime(2020, 6, 9, 10, 47, 24, 659, DateTimeKind.Local).AddTicks(8340) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(998), "https://picsum.photos/640/480/?image=630", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(1764) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2121), "https://picsum.photos/640/480/?image=84", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2201) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2227), "https://picsum.photos/640/480/?image=390", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2232) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2252), "https://picsum.photos/640/480/?image=319", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2257) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2276), "https://picsum.photos/640/480/?image=863", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2281) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2391), "https://picsum.photos/640/480/?image=88", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2396) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2416), "https://picsum.photos/640/480/?image=482", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2420) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2438), "https://picsum.photos/640/480/?image=1021", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2443) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2461), "https://picsum.photos/640/480/?image=506", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2465) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2482), "https://picsum.photos/640/480/?image=946", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2487) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2504), "https://picsum.photos/640/480/?image=380", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2508) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2525), "https://picsum.photos/640/480/?image=656", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2530) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2547), "https://picsum.photos/640/480/?image=343", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2551) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2568), "https://picsum.photos/640/480/?image=621", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2573) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2590), "https://picsum.photos/640/480/?image=708", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2595) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2612), "https://picsum.photos/640/480/?image=118", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2617) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2634), "https://picsum.photos/640/480/?image=697", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2638) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2655), "https://picsum.photos/640/480/?image=113", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2659) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2954), "https://picsum.photos/640/480/?image=90", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2959) });

            migrationBuilder.UpdateData(
                table: "Images",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "URL", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2980), "https://picsum.photos/640/480/?image=559", new DateTime(2020, 6, 9, 10, 47, 24, 667, DateTimeKind.Local).AddTicks(2984) });

            migrationBuilder.InsertData(
                table: "PostReactions",
                columns: new[] { "Id", "CreatedAt", "IsLike", "PostId", "UpdatedAt", "UserId" },
                values: new object[,]
                {
                    { 10, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9864), true, 6, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9869), 1 },
                    { 11, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9889), false, 6, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9894), 9 },
                    { 12, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9915), true, 13, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9919), 8 },
                    { 13, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9940), false, 20, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9945), 19 },
                    { 15, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9995), false, 8, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9999), 4 },
                    { 18, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(71), false, 10, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(76), 14 },
                    { 16, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(21), false, 13, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(26), 2 },
                    { 17, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(46), true, 18, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(51), 3 },
                    { 9, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9838), false, 16, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9843), 8 },
                    { 20, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(122), true, 8, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(126), 1 },
                    { 19, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(96), false, 10, new DateTime(2020, 6, 9, 10, 47, 24, 869, DateTimeKind.Local).AddTicks(101), 4 },
                    { 14, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9965), true, 13, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9970), 10 },
                    { 8, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9811), false, 15, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9816), 10 },
                    { 3, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9674), false, 20, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9679), 1 },
                    { 6, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9758), true, 5, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9762), 20 },
                    { 5, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9730), false, 10, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9735), 9 },
                    { 1, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(8601), false, 1, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9042), 20 },
                    { 4, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9703), true, 18, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9708), 1 },
                    { 2, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9622), false, 19, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9638), 16 },
                    { 7, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9785), true, 1, new DateTime(2020, 6, 9, 10, 47, 24, 868, DateTimeKind.Local).AddTicks(9789), 16 }
                });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Est veniam vero error sequi dignissimos iure occaecati.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(1574), 32, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(2042) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 7, @"Cupiditate et eaque nulla et molestiae nostrum sed qui.
Repellendus qui dolore repellendus nemo quia veritatis architecto nihil excepturi.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5179), 32, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5211) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 14, @"Voluptatem debitis enim doloremque.
Voluptatem expedita et ut quis perspiciatis occaecati.
Minima vel amet sed.
Vel quasi dolorem.
Distinctio vitae ut occaecati aut eius et dolor non enim.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5575), 28, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5584) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 17, "Sit molestias quia rerum magni delectus minima.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5660), 30, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5666) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, @"Quae voluptas totam ut aliquam sunt sit et numquam.
Soluta doloremque neque odio.
Autem beatae enim consequuntur culpa illum dolorem dolorem laborum accusamus.
Earum velit harum neque ipsam eligendi cumque nemo.
Ipsum impedit earum perferendis.
Facilis eius suscipit dolorem dolorem qui voluptatum eligendi doloremque.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5979), 30, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(5987) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 18, "Ut reiciendis eius nesciunt vero in sint.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6056), 33, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6062) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, @"Doloribus sit vero ut omnis vitae dolores aliquam corporis itaque.
Corrupti nisi temporibus est voluptatem magnam ipsum.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6228), 23, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(6234) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 16, "Voluptas hic perferendis at perspiciatis et. Dolorem est veniam hic. Vel dolorum molestiae enim ea quis ut modi ullam.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7152), 28, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7167) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Magnam vel est totam ipsam ab dolorem et. Ipsam placeat tempora officia possimus fugit aut. Quia reprehenderit tenetur repudiandae facilis dolorem ut quod qui officia.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7388), 39, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7395) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { "Repellendus officiis magnam consequatur. Qui ut eveniet aut. Sunt aut occaecati quia optio deleniti aut amet in non.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7561), 29, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7568) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 3, "Placeat est eos praesentium aut optio officia. Unde odio porro. Corporis recusandae nulla ut dolores earum delectus. Quo vel error facere aut eum voluptatibus. Eligendi omnis tempore fugit est aliquam et.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7751), 29, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7757) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Magnam et perferendis aut.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7846), 35, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(7853) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 9, "Omnis nostrum molestiae ipsum autem. Nulla consequatur neque. Aliquid ab porro. Ea assumenda assumenda. At ab quas et consequuntur illo illum. Inventore optio voluptate eum est dolores voluptatem qui.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8076), 40, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8082) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 21, "Animi voluptate veritatis aliquam alias. Architecto provident blanditiis. Sed delectus eum quia omnis placeat quia. Accusamus ex architecto doloribus. Sapiente sit suscipit numquam.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8236), 21, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8243) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "alias", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8511), 39, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8523) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 4, "Rerum odio quia quia delectus.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8664), 35, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8672) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Et iure perferendis voluptatem.
Accusamus labore voluptatibus qui dolore laborum quaerat aut et libero.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8794), 38, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8801) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 1, @"Veniam optio odit et sed nemo rerum.
Et et necessitatibus.
Et omnis totam rerum sed voluptas.
Sit voluptas itaque.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8969), 23, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(8976) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 12, @"Magnam qui explicabo eligendi rerum.
Voluptatum sint dolorem veniam.
Ratione tempore molestiae adipisci.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9074), 22, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9080) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "Body", "CreatedAt", "PreviewId", "UpdatedAt" },
                values: new object[] { 6, "Consectetur nisi blanditiis est.", new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9130), 34, new DateTime(2020, 6, 9, 10, 47, 24, 856, DateTimeKind.Local).AddTicks(9136) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 709, DateTimeKind.Local).AddTicks(1112), "Marge.Jacobson79@hotmail.com", "dZUDP17KvlBCYw1DPNTmIPRb2SVUA8d/BHKxuYoSwjg=", "wG5dtTlf1vtbhFjbd0InjOp58d5fokcbRV8sWq4rApw=", new DateTime(2020, 6, 9, 10, 47, 24, 709, DateTimeKind.Local).AddTicks(1738), "Tyreek49" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 9, 10, 47, 24, 716, DateTimeKind.Local).AddTicks(3468), "Marietta_Paucek@gmail.com", "2VRUBKnnWOO3XOjWFkeQPOEyWVtTG34+cTM+M84KsOc=", "IccC7Lm0YPoe+aZZISIXlARjvFb9CKhGxJ2FqxWDyNg=", new DateTime(2020, 6, 9, 10, 47, 24, 716, DateTimeKind.Local).AddTicks(3565), "Elvera_Bernier39" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 9, 10, 47, 24, 723, DateTimeKind.Local).AddTicks(3701), "Zoie22@yahoo.com", "1fsbhUd06TyRmP4S4qXORn8pqqyUCtgWaaLU6fKPdHE=", "o9ri56eMArjs/6CbX/GOkz/Sein2b2RWGBx0meyzTcQ=", new DateTime(2020, 6, 9, 10, 47, 24, 723, DateTimeKind.Local).AddTicks(3775), "Forrest_Shields24" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 10, 47, 24, 732, DateTimeKind.Local).AddTicks(1357), "Dudley76@yahoo.com", "A4xQLMNyuaAI4/xPCi+dQV9Z9MXSvkIFOpV9nYWVqsc=", "RPSLSnOGp63EmIdbm9iHt7hzLw+JWlHb2nKgEpRWKbY=", new DateTime(2020, 6, 9, 10, 47, 24, 732, DateTimeKind.Local).AddTicks(1443), "Melyssa.OKon" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 20, new DateTime(2020, 6, 9, 10, 47, 24, 739, DateTimeKind.Local).AddTicks(719), "Brendan_Gutmann@hotmail.com", "cWW4PGnXn9AzJH1O/rSIsJ2Qcaqy8gFWi701NuYf0dY=", "7vWDK3wAptvcFvdwP2r5/Y7wXJvuGEeb4gRYTj4zYII=", new DateTime(2020, 6, 9, 10, 47, 24, 739, DateTimeKind.Local).AddTicks(767), "Joseph_Collins28" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 18, new DateTime(2020, 6, 9, 10, 47, 24, 745, DateTimeKind.Local).AddTicks(9844), "Sharon_Bailey@gmail.com", "IWgRlvbzAqNP1L38TVWTp8TEBJsSrRzX/CFX00F40TU=", "3Uon4xHMlEtQl1UcDJgM8xkjjo8pkyiXtEkKwlu5ECo=", new DateTime(2020, 6, 9, 10, 47, 24, 745, DateTimeKind.Local).AddTicks(9895), "Shanel_Crooks" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 9, 10, 47, 24, 753, DateTimeKind.Local).AddTicks(734), "Jane98@hotmail.com", "A3JrpvKOVB2lqqw0XiylFc9ZSeDA4ncnWnUJx2+KgUw=", "0Is6PBq1hDmFDmJcSSnS0Vv4ViQNmNcTqQiA5OADXd8=", new DateTime(2020, 6, 9, 10, 47, 24, 753, DateTimeKind.Local).AddTicks(792), "Nigel_Olson87" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 15, new DateTime(2020, 6, 9, 10, 47, 24, 759, DateTimeKind.Local).AddTicks(9638), "Cleta_Hoppe@gmail.com", "4wWzWTNJOSGhmP9ITIdp0nnRTqyj4GKZhJHSVwNb4iE=", "ZrXiCe42TtMhI49dGBC5U4USCWgOPcUb99ZWIh5wYW0=", new DateTime(2020, 6, 9, 10, 47, 24, 759, DateTimeKind.Local).AddTicks(9662), "Idella47" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 8, new DateTime(2020, 6, 9, 10, 47, 24, 766, DateTimeKind.Local).AddTicks(8370), "Sanford_Erdman16@hotmail.com", "dZYl+NRpKo9pnhli2miWe2ZiZS+rtoos98eYZnYHzlI=", "j7xukoVYWJc7nvgeoG/KZckk3RinaysNVseVdD8aH6g=", new DateTime(2020, 6, 9, 10, 47, 24, 766, DateTimeKind.Local).AddTicks(8389), "Glennie79" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 10, 47, 24, 773, DateTimeKind.Local).AddTicks(6716), "Arlene_Durgan@hotmail.com", "imAT0g4b4jt8OUE2Fk4Q812qRMHzR+yvLbzrcroH5Bg=", "Ncms2T6vb+ZkQVrG9p+jhDsIHeNQytZLyAarj2OP42Q=", new DateTime(2020, 6, 9, 10, 47, 24, 773, DateTimeKind.Local).AddTicks(6746), "Antonietta.Hansen74" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 11, new DateTime(2020, 6, 9, 10, 47, 24, 780, DateTimeKind.Local).AddTicks(5888), "Peter8@yahoo.com", "Nbs+x7YkqWZLYkVWrZMaSrxBOzIXc8ck9RO8WYqlmj0=", "rEMDaIqNGqr8Mv7mb+KE+mgLlRnge5l/eD/xmLnfj3k=", new DateTime(2020, 6, 9, 10, 47, 24, 780, DateTimeKind.Local).AddTicks(5960), "Niko_Abshire" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 19, new DateTime(2020, 6, 9, 10, 47, 24, 787, DateTimeKind.Local).AddTicks(4511), "Winston22@hotmail.com", "8fGRH8+4DQx9QVIpeemxlafPkbFsotOstUHEQlFJgBs=", "2QQaCBOEfBEMCyklUYTp5kg6w+gVvgBON+t+Qhn45SM=", new DateTime(2020, 6, 9, 10, 47, 24, 787, DateTimeKind.Local).AddTicks(4527), "Nolan_Shanahan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 13, new DateTime(2020, 6, 9, 10, 47, 24, 794, DateTimeKind.Local).AddTicks(5791), "Gerardo76@hotmail.com", "a3Xoxs8zerVkXYStY5TcK1MTzXCGdgDRAAMZI+UJfpw=", "jnjIGvPth+A553tenTnV55zlrt+KtAgXF5U7L2lrafE=", new DateTime(2020, 6, 9, 10, 47, 24, 794, DateTimeKind.Local).AddTicks(5858), "Kali.Lockman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 1, new DateTime(2020, 6, 9, 10, 47, 24, 801, DateTimeKind.Local).AddTicks(4578), "Sigmund78@yahoo.com", "iTS+mVVn+fSkirlqcMIgoNfWXNKuTctbPF1vfSytwmI=", "JR0RYpjVdaLbejTq5Vq0ReEqT/l22oAI8Ka9LHUjetg=", new DateTime(2020, 6, 9, 10, 47, 24, 801, DateTimeKind.Local).AddTicks(4602), "Dasia_Gorczany67" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 10, new DateTime(2020, 6, 9, 10, 47, 24, 808, DateTimeKind.Local).AddTicks(3072), "Emiliano80@gmail.com", "XFuEurLYlkZ1EAM5hNzQydpDc7vC5poH8PHebW7rAVY=", "ZG5PlwR/CLWac61bdYO1dGK++l40WqYdmRiTv1S+CXk=", new DateTime(2020, 6, 9, 10, 47, 24, 808, DateTimeKind.Local).AddTicks(3096), "Ross.Douglas57" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 14, new DateTime(2020, 6, 9, 10, 47, 24, 815, DateTimeKind.Local).AddTicks(1364), "Terrance.Glover59@hotmail.com", "7ji0jGgbFXakHnGw/fkloDqxNYgIEP5Azb8m3ANbvUA=", "od6A5Km7KVDWyrpC1P6vwI5v0GakrmXo057rC96zxZg=", new DateTime(2020, 6, 9, 10, 47, 24, 815, DateTimeKind.Local).AddTicks(1383), "Bernadine_Conn" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 6, new DateTime(2020, 6, 9, 10, 47, 24, 821, DateTimeKind.Local).AddTicks(9570), "Marlen17@gmail.com", "ftu/5O6gln8A63EqFftOSyXD6B+TqfoNMrLUcBZkIRo=", "U/Uvf+fEu7ABaOiaW1z/Kr8jkFab/T/JDutJCee4G0U=", new DateTime(2020, 6, 9, 10, 47, 24, 821, DateTimeKind.Local).AddTicks(9632), "Chelsey_Nader" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 9, new DateTime(2020, 6, 9, 10, 47, 24, 828, DateTimeKind.Local).AddTicks(7799), "Patrick_Schamberger@hotmail.com", "csrCfvuhj1nUWVSSRpf8Zt6v6FjaVXxA4lX93uZCz40=", "NcJSxlMEmRQq+v8GSyANLwBPuklgHnFOvTJWP/M8Y9s=", new DateTime(2020, 6, 9, 10, 47, 24, 828, DateTimeKind.Local).AddTicks(7822), "Mireille83" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 7, new DateTime(2020, 6, 9, 10, 47, 24, 835, DateTimeKind.Local).AddTicks(6229), "Nannie_Nicolas@yahoo.com", "p/zqpKxZyhXzUlRiEkhXWy1C2AEDEHS8wjIhoXfy63c=", "RSKl055WOfJD0oKcSciJHLWX5NQYcFUCpWlm0eI5A7k=", new DateTime(2020, 6, 9, 10, 47, 24, 835, DateTimeKind.Local).AddTicks(6258), "Drake_Boyle55" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AvatarId", "CreatedAt", "Email", "Password", "Salt", "UpdatedAt", "UserName" },
                values: new object[] { 17, new DateTime(2020, 6, 9, 10, 47, 24, 842, DateTimeKind.Local).AddTicks(4957), "Carleton98@hotmail.com", "iiiAtgvxsTSN/J92thkn3sfU/JK/G7GgL0fZ76yW7M8=", "NBZCPHkScDEfsS/urHTbx3TpwIEwHc5tt+bg4elyBKg=", new DateTime(2020, 6, 9, 10, 47, 24, 842, DateTimeKind.Local).AddTicks(4994), "Ashlee_Spencer75" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Password", "Salt", "UpdatedAt" },
                values: new object[] { new DateTime(2020, 6, 9, 10, 47, 24, 849, DateTimeKind.Local).AddTicks(3120), "sQZ05gjLn48vR2odYqMsn8r2EfBLvjumxuwmfkv6yIE=", "qCuJMype13uWetAbTWrs3NHnXp8M3qkJb8GV4QAPpVw=", new DateTime(2020, 6, 9, 10, 47, 24, 849, DateTimeKind.Local).AddTicks(3120) });
        }
    }
}
