import { Injectable } from "@angular/core";
import { HttpInternalService } from "./http-internal.service";
import { Post } from "../models/post/post";
import { NewReaction } from "../models/reactions/newReaction";
import { NewPost } from "../models/post/new-post";
import { SharePost } from '../models/post/share-post';

@Injectable({ providedIn: "root" })
export class PostService {
  postIdData: any;

  public routePrefix = "/api/posts";

  constructor(private httpService: HttpInternalService) { }

  public getPosts() {
    return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
  }
  public getPost(id: number) {
    return this.httpService.getFullRequest<Post>(`${this.routePrefix}/` + id);
  }

  public createPost(post: NewPost) {
    return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
  }

  public likePost(reaction: NewReaction) {
    return this.httpService.postFullRequest<Post>(
      `${this.routePrefix}/like`,
      reaction
    );
  }
  public deletePost(id: number) {
    return this.httpService.deleteFullRequest<Post>(`${this.routePrefix}/` + id);
  }
  public updatePost(post: Post) {
    return this.httpService.putFullRequest<Post>(`${this.routePrefix}`, post);
  }

  public sharePost(sharePost: SharePost) {
    return this.httpService.postFullRequest<Post>(`${this.routePrefix}/share`, sharePost);
  }


}
