import { Component, Input, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { PostService } from 'src/app/services/post.service';
import { NewPost } from 'src/app/models/post/new-post';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Post } from 'src/app/models/post/post';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { takeUntil, switchMap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ImgurService } from 'src/app/services/imgur.service';

@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  ngOnInit(): void {
    this.postService.getPost(this.id).pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.post = resp.body;
        });
  }

  private unsubscribe$ = new Subject<void>();

  @Input() id: number;

  myForm: FormGroup;
  newPost = {} as NewPost;
  post = {} as Post;
  public imageFile: File;
  public loading = false;
  constructor(
    public activeModal: NgbActiveModal,
    private postService: PostService,
    private snackBarService: SnackBarService,
    private imgurService: ImgurService
  ) { }

  public handleFileInput(target: any) {
    this.imageFile = target.files[0];

    if (!this.imageFile) {
      target.value = '';
      return;
    }

    if (this.imageFile.size / 1000000 > 5) {
      this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
      target.value = '';
      return;
    }

    const reader = new FileReader();
    reader.addEventListener('load', () => (this.post.previewImage = reader.result as string));
    reader.readAsDataURL(this.imageFile);
  }

  public saveNewInfo() {
    const postSubscription = !this.imageFile
      ? this.postService.updatePost(this.post)
      : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
        switchMap((imageData) => {
          this.post.previewImage = imageData.body.data.link;
          return this.postService.updatePost(this.post);
        })
      );

    this.loading = true;

    postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
      () => {
        this.snackBarService.showUsualMessage('Successfully updated');
        this.loading = false;
      },
      (error) => this.snackBarService.showErrorMessage(error)
    );

    this.activeModal.close(this.post);

  }
  close() {
    this.activeModal.close();
  }

}