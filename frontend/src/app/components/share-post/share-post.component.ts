import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PostService } from 'src/app/services/post.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { ImgurService } from 'src/app/services/imgur.service';
import { User } from 'src/app/models/user';
import { SharePost } from 'src/app/models/post/share-post';

@Component({
    selector: 'app-share-post',
    templateUrl: './share-post.component.html',
    styleUrls: ['./share-post.component.css']
})
export class SharePostComponent {
    @Input() id: number;
    @Input() public currentUserId: number;
    public email: string;

    constructor(
        public activeModal: NgbActiveModal,
        private postService: PostService,
        private snackBarService: SnackBarService,
    ) { }

    share() {
        let sharePost: SharePost = {
            currentUserId: this.currentUserId,
            postId: this.id,
            email: this.email
        };

        this.postService.sharePost(sharePost).subscribe(() => {
            this.snackBarService.showUsualMessage('Post was shared');
        },
            (error) => this.snackBarService.showErrorMessage(error));

        this.activeModal.close();
    }

    close() {
        this.activeModal.close();
    }
}