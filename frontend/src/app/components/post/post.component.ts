import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from "@angular/core";
import { Post } from "../../models/post/post";
import { AuthenticationService } from "../../services/auth.service";
import { AuthDialogService } from "../../services/auth-dialog.service";
import { empty, Observable, Subject } from "rxjs";
import { DialogType } from "../../models/common/auth-dialog-type";
import { LikeService } from "../../services/like.service";
import { NewComment } from "../../models/comment/new-comment";
import { CommentService } from "../../services/comment.service";
import { User } from "../../models/user";
import { Comment } from "../../models/comment/comment";
import { catchError, switchMap, takeUntil } from "rxjs/operators";
import { SnackBarService } from "../../services/snack-bar.service";
import { PostService } from "src/app/services/post.service";
import { NewPost } from '../../models/post/new-post';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditPostComponent } from '../edit-post/edit-post.component';
import { Reaction } from 'src/app/models/reactions/reaction';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { SharePostComponent } from '../share-post/share-post.component';

@Component({
  selector: "app-post",
  templateUrl: "./post.component.html",
  styleUrls: ["./post.component.sass"],
})
export class PostComponent implements OnDestroy {
  @Input() public post: Post;
  @Input() public currentUser: User;
  @Input() public cashedPosts: Post[];
  @Output() postDeleted: EventEmitter<any> = new EventEmitter();


  public showComments = false;
  public newComment = {} as NewComment;
  public posts: Post[];
  private unsubscribe$ = new Subject<void>();
  public show: boolean = false;
  public postHub: HubConnection;

  public constructor(
    private authService: AuthenticationService,
    private authDialogService: AuthDialogService,
    private likeService: LikeService,
    private commentService: CommentService,
    private snackBarService: SnackBarService,
    private postService: PostService,
    private modalService: NgbModal,
  ) {
  }

  // public ngOnInit() {
  //   this.registerHub();
  // }
  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public onToggle(): void {
    this.show = !this.show;
  }
  public likes(): Reaction[] {
    return this.post.reactions.filter(x => x.isLike == true);
  }
  public disLikes(): Reaction[] {
    return this.post.reactions.filter(x => x.isLike == false);
  }

  public toggleComments() {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((user) => {
          if (user) {
            this.currentUser = user;
            this.showComments = !this.showComments;
          }
        });
      return;
    }

    this.showComments = !this.showComments;
  }

  public likePost() {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(
          switchMap((userResp) =>
            this.likeService.likePost(this.post, userResp)
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe((post) => (this.post = post));

      return;
    }

    this.likeService
      .likePost(this.post, this.currentUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((post) => (this.post = post));
  }

  public disLikePost() {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(
          switchMap((userResp) =>
            this.likeService.disLikePost(this.post, userResp)
          ),
          takeUntil(this.unsubscribe$)
        )
        .subscribe((post) => (this.post = post));

      return;
    }

    this.likeService
      .disLikePost(this.post, this.currentUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((post) => (this.post = post));
  }

  public sendComment() {
    this.newComment.authorId = this.currentUser.id;
    this.newComment.postId = this.post.id;

    this.commentService
      .createComment(this.newComment)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          if (resp) {
            this.post.comments = this.sortCommentArray(
              this.post.comments.concat(resp.body)
            );
            this.newComment.body = undefined;
          }
        },
        (error) => this.snackBarService.showErrorMessage(error)
      );
  }

  public openAuthDialog() {
    this.authDialogService.openAuthDialog(DialogType.SignIn);
  }

  private catchErrorWrapper(obs: Observable<User>) {
    return obs.pipe(
      catchError(() => {
        this.openAuthDialog();
        return empty();
      })
    );
  }

  private sortCommentArray(array: Comment[]): Comment[] {
    return array.sort(
      (a, b) => +new Date(b.createdAt) - +new Date(a.createdAt)
    );
  }
  public deletePost(id: number) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(takeUntil(this.unsubscribe$)).subscribe();

      return;
    }

    var post = this.postService.deletePost(id).subscribe((res) => {
      this.postDeleted.emit();
    });
  }

  public openEditPost(id: number) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(takeUntil(this.unsubscribe$)).subscribe();

      return;
    }

    const modalRef = this.modalService.open(EditPostComponent);
    modalRef.componentInstance.id = id;

    modalRef.result.then((result) => {
      if (result) { this.post = result; }
    }).catch((error) => {
      console.log(error);
    });
  }

  onCommentDeleted(comment) {
    var index = this.post.comments.findIndex((c) => (c === comment));
    if (index !== -1) {
      this.post.comments.splice(index, 1);
    }
  }

  public openSharePost(id: number) {
    if (!this.currentUser) {
      this.catchErrorWrapper(this.authService.getUser())
        .pipe(takeUntil(this.unsubscribe$)).subscribe();

      return;
    }

    const modalRef = this.modalService.open(SharePostComponent);
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.currentUserId = this.currentUser.id;

    modalRef.result.then((result) => {
      if (result) { this.post = result; }
    }).catch((error) => {
      console.log(error);
    });
  }

}
