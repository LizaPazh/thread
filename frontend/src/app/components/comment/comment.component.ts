import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { User } from 'src/app/models/user';
import { Observable, empty, Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { AuthenticationService } from 'src/app/services/auth.service';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { LikeService } from 'src/app/services/like.service';
import { CommentService } from 'src/app/services/comment.service';
import { Reaction } from 'src/app/models/reactions/reaction';


@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() commentDeleted: EventEmitter<any> = new EventEmitter();

    private unsubscribe$ = new Subject<void>();
    public editable: boolean = false;

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService) { }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    public likes(): Reaction[] {
        return this.comment.reactions.filter(x => x.isLike == true);
    }
    public disLikes(): Reaction[] {
        return this.comment.reactions.filter(x => x.isLike == false);
    }
    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.likeComment(this.comment, userResp)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }
    }
    public disLikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) =>
                        this.likeService.disLikeComment(this.comment, userResp)
                    ),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => (this.comment = comment));

            return;
        }
    }
    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }
    public updateComment(comment: Comment) {
        const commentSubscription = this.commentService.updateComment(this.comment);

        commentSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(() => {
            this.comment = comment;
            this.editable = false;
        });
    }
    public deleteComment(id: number) {
        this.commentService.deleteComment(id).subscribe((res) => {
            this.commentDeleted.emit();
        });
    }
}
