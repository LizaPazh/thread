export interface SharePost {
    postId: number;
    currentUserId: number;
    email: string;
}